import { AfterViewInit } from '@angular/core';
import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-home1',
  templateUrl: './home1.component.html',
  styleUrls: ['./home1.component.css']
})
export class Home1Component implements OnInit, AfterViewInit {

  readonly logos = [
    {
      imgSrc: "assets/images/logo/1.png"
    },{
      imgSrc: "assets/images/logo/2.png"
    },{
      imgSrc: "assets/images/logo/3.png"
    },{
      imgSrc: "assets/images/logo/4.png"
    },{
      imgSrc: "assets/images/logo/5.png"
    },{
      imgSrc: "assets/images/logo/6.png"
    },{
      imgSrc: "assets/images/logo/7.png"
    },{
      imgSrc: "assets/images/logo/8.png"
    },{
      imgSrc: "assets/images/logo/9.png"
    },{
      imgSrc: "assets/images/logo/10.png"
    },{
      imgSrc: "assets/images/logo/11.png"
    },
  ]

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
  }
}
