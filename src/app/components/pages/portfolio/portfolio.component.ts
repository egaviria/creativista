import { AfterViewInit, Component, OnInit } from '@angular/core';
import { BANNER_ITEMS, FLYER_ITEMS, ITEMS_CESAR, LOGO_ITEMS, MAQUILA_ITEMS } from './items';

declare var $: any;



export function shuffle(array) {
  return array.sort(() => Math.random() - 0.5);
}


@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit, AfterViewInit {

  readonly items = [
    ...MAQUILA_ITEMS,
    ...BANNER_ITEMS,
    ...FLYER_ITEMS,
    ...LOGO_ITEMS,
    ...ITEMS_CESAR,
  ];



  constructor() {
    this.items = shuffle(this.items);
  }

  ngAfterViewInit(): void {
    setTimeout(()=>{
      $.getScript('assets/js/custom.js');
    }, 500)
  }

  ngOnInit(): void {
  }



}
