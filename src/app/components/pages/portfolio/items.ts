export const MAQUILA_ITEMS: any[] = [
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila1.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila2.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila3.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila4.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila5.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila6.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila7.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila8.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila9.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila10.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila11.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila12.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila13.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila14.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila15.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila16.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila17.jpg',
    },
    {
        classFilter: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila18.jpg',
    },
    {
        classFiltere: 'maquila',
        imgSrc: 'assets/images/maquilas/maquila19.jpg',
    }
];

export const BANNER_ITEMS: any[] = [
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner1.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner2.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner3.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner4.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner5.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner6.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner7.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner8.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner9.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner10.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner11.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner12.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner13.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner14.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner15.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner16.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner17.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner18.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner19.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner20.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner21.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner22.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner23.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner24.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner25.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner26.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner27.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner28.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner29.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner30.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner31.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner32.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner33.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner34.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner35.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner36.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner37.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner38.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner39.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner40.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner41.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner42.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner43.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner44.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner45.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner46.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner47.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner48.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner49.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner50.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner51.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner52.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner53.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner54.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner55.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner56.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner57.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner58.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner59.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner60.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner61.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner62.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner63.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner64.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner65.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner66.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner67.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner68.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner69.jpg',
    },

    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner70.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner71.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner72.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner73.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner74.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner75.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner76.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner77.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner78.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner79.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner80.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner81.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner82.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner83.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner84.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner85.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner86.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner87.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner88.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner89.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner90.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner91.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner92.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner93.jpg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/banner/banner94.jpg',
    },
    {
        classFiltere: 'banner',
        imgSrc: 'assets/images/banner/banner95.jpg',
    }
];

export const FLYER_ITEMS: any[] = [
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer1.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer3.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer4.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer5.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer7.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer8.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer9.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer10.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer11.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer12.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer14.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer15.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer16.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer17.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer18.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer19.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer20.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer21.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer22.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer23.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer24.jpg',
    },

    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer25.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer26.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer27.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer28.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer29.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer30.jpg',
    },
    {
        classFilter: 'flyer',
        imgSrc: 'assets/images/flyers/flyer31.jpg',
    },
    {
        classFiltere: 'flyer',
        imgSrc: 'assets/images/flyers/flyer32.jpg',
    }
];

export const LOGO_ITEMS: any[] = [
    {
        imgSrc: 'assets/images/logos/logo1.jpg',
        name: 'Creativista',
        description: 'Hotel Bella vista',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo2.jpg',
        name: 'Creativista',
        description: 'Empresa IBG',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo3.jpg',
        name: 'Creativista',
        description: 'Constructora',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo4.jpg',
        name: 'Creativista',
        description: 'Marca de accesorios',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo5.jpg',
        name: 'Creativista',
        description: 'Pagina Web',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo6.jpg',
        name: 'Creativista',
        description: 'Constructora',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo7.jpg',
        name: 'Creativista',
        description: 'Image Description',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo8.jpg',
        name: 'Creativista',
        description: 'Marca de accesorios',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo9.jpg',
        name: 'Creativista',
        description: 'Casa cultural',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo10.jpg',
        name: 'Creativista',
        description: 'Página Web',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo11.jpg',
        name: 'Creativista',
        description: 'Mascota centro comercial',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo12.jpg',
        name: 'Creativista',
        description: 'Productos de aseo',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo13.jpg',
        name: 'Creativista',
        description: 'Accesorios & Joyería',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo14.jpg',
        name: 'Creativista',
        description: 'Fonda',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo15.jpg',
        name: 'Creativista',
        description: 'Hotel Copacabana',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo16.jpg',
        name: 'Creativista',
        description: 'Productos de aseo',
        classFilter: 'logo',
    },

    {
        imgSrc: 'assets/images/logos/logo17.jpg',
        name: 'Creativista',
        description: 'Productos de aseo',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo18.jpg',
        name: 'Creativista',
        description: 'Productos de aseo',
        classFilter: 'logo',
    }, {
        imgSrc: 'assets/images/logos/logo19.jpg',
        name: 'Creativista',
        description: 'Productos de aseo',
        classFilter: 'logo',
    },
];

export const ITEMS_CESAR: any[] = [
    {
        imgSrc: 'assets/images/portfolio/1.jpg',
        name: 'Creativista',
        description: 'Lily likes to play with crayons and pencils',
        classFilter: 'banner',
    }, {
        imgSrc: 'assets/images/portfolio/2.jpg',
        name: 'Creativista',
        description: 'Lily likes to play with crayons and pencils',
        classFilter: 'flyer',
    }, {
        imgSrc: 'assets/images/portfolio/3.jpg',
        name: 'Creativista',
        description: 'Lily likes to play with crayons and pencils',
        classFilter: 'banner',
    }, {
        imgSrc: 'assets/images/portfolio/4.jpg',
        name: 'Creativista',
        description: 'Lily likes to play with crayons and pencils',
        classFilter: 'flyer',
    }, {
        imgSrc: 'assets/images/portfolio/5.jpg',
        name: 'Creativista',
        description: 'Lily likes to play with crayons and pencils',
        classFilter: 'banner',
    }, {
        imgSrc: 'assets/images/portfolio/6.jpg',
        name: 'Creativista',
        description: 'Lily likes to play with crayons and pencils',
        classFilter: 'flyer',
    },

    {
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner96.jpeg',
    },
    {
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner97.jpeg',
    },{
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner98.jpeg',
    },{
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner99.jpeg',
    },{
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner100.jpeg',
    },{
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner101.jpeg',
    },{
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner102.jpeg',
    },{
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner103.jpeg',
    },{
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner104.jpeg',
    },{
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner105.jpeg',
    },{
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner106.jpeg',
    },{
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner107.jpeg',
    },{
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner108.jpeg',
    },{
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner109.jpeg',
    },{
        classFilter: 'banner',
        imgSrc: 'assets/images/portfolio/banner110.jpeg',
    },
]