import { AfterViewInit, Component } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'creativista';

  ngAfterViewInit(): void {
    $.getScript('assets/js/numscroller.js');
  }
}
